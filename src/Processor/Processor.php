<?php
namespace Processor;

use Git\Client;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class Processor
{
    private $client;
    private $progress;
    private $output;

    public function __construct(Client $client, ProgressBar $progress = null, OutputInterface $output = null)
    {
        $this->client = $client;
        $this->progress = $progress;
        $this->output = $output;
    }

    public function process($config)
    {
        $byProject = [];
        $allUsers = [];

        $startDate = new \DateTime($config['config']['startDate']);
        $endDate = new \DateTime($config['config']['endDate']);

        foreach($config['repositories'] as $name => $remote) {
            if(null !== $this->progress) $this->progress->setMessage($name);
            
            try {
                $id = $this->client->grab($remote);
            }
            catch(\Exception $e) {
                if(null !== $this->output) $this->output->writeln("<error>Problem fetching repository $name [$remote] : " . $e->getMessage() . "</error>");
                continue;
            }
            
            $log = $this->client->log($id);

            $byUser = [];
            
            $workBlocks = [
                'number' => 0,
                'largest' => 0,
            ];

            $lastDate = null;
            $contiguous = 1;
            $lines = explode(PHP_EOL, $log);
            foreach($lines as $line) {

                list($date,$user,$message) = explode('|', $line);

                // skip any release messages
                if(0 === strpos($message, 'Released'))
                    continue;

                // only deal with messages inside the window
                $dateObj = new \DateTime($date);

                if($dateObj < $startDate || $dateObj > $endDate || $dateObj == $lastDate)
                    continue;

                if(!isset($byUser[$user]))
                    $byUser[$user] = [];

                if(!isset($byUser[$user][$date]))
                    $byUser[$user][$date] = 1;

                if(!isset($allUsers[$user]))
                    $allUsers[$user] = count($allUsers);

                // look for contiguous blocks of commits
                if(null !== $lastDate && $lastDate->diff($dateObj)->days < 5) {
                    // in the middle of a run
                    $contiguous++;
                } else {
                    // at the end of a run
                    if($workBlocks['largest'] < $contiguous) {
                        $workBlocks['largest'] = $contiguous;
                    }

                    $workBlocks['number']++;
                    $contiguous = 1;
                }

                $lastDate = $dateObj;
            }

            $byProject[$name] = [];

            $first = end($lines);
            $last = reset($lines);

            list($byProject[$name]['first']) = explode('|', $first);
            list($byProject[$name]['last']) = explode('|', $last);

            $byProject[$name]['counts'] = $byUser;
            $byProject[$name]['workBlocks'] = $workBlocks;
            if(null !== $this->progress) $this->progress->advance();
        }

        return [
            'allUsers' => $allUsers,
            'byProject' => $byProject,
        ];
    }

}
## Git Effort Log

Finds the number of days spent by each contributor to a set of git repositories in a given period.

The calculation is a crude one based on the assumption that any day on which a commit was made was an entire day spent working on that project. This is obviously not always going the be the case and will lead to overestimated figures but they can still be used to make rough estimates and to do comparisons between projects.

# Installation

Clone the repository and run composer install

# Usage

First, write a configuration file (see config/sample.json):

{
    "config": {
        "startDate": "2016-01-01",
        "endDate": "2016-12-31"
    },
    "repositories": {
        "git-effort-log": "git@bitbucket.org:dan_d_martin/git-effort-log.git"
    }
}


Under the config key there should be two values defined: startDate and endDate. These define the start and end of the period for which data is to be collated. They are mandatory but can always be set to a range that encompasses the whole life of the repositories.

Under the repositories key you should define your projects. The key is the name of the project and the value is the repo containing the project.

Once the file is defined, you can run the command as follows:

bin/effort estimate config/sample.json

The repositories defined in the config file will be fetched into the cache directory (so you will need to have git installed and valid keys for accessing any private repositories) and their logs are analysed.

when the process is complete you will see output like this:

+----------------+--------------+-------------+------------------+---------------+------------------------------------+
| project        | first commit | last commit | number of blocks | largest block | dan.martin@epiphanysolutions.co.uk |
+----------------+--------------+-------------+------------------+---------------+------------------------------------+
| git-effort-log | 2016-01-04   | 2016-01-07  | 1                | 1             | 3                                  |
+----------------+--------------+-------------+------------------+---------------+------------------------------------+

Project, first commit and last commit are all self-explanatory.

Number of blocks refers to the number of blocks of consecutive daily commits (i.e. where a project was worked on for several days in a row)
Largest block is the number of days spent during the longest of these.

The remaining columns show each user who worked on the repositories and the number of days they worked on it for.

# TODO

- The cache directory is never cleared out and it should be

- Better estimates could be produced by looking at the time of commits as well as the date
<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;

use Git\Client;
use Processor\Processor;

class GitEffortCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('estimate')
            ->setDescription('Estimate the time spent on a git repo by each user')
            ->addArgument('config',InputArgument::REQUIRED, 'Configuration file - json format')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configFile = $input->getArgument('config');
        
        $config = json_decode(file_get_contents($configFile), true);

        $client = new Client("cache");

        $progress = new ProgressBar($output, count($config['repositories']));
        $progress->setBarCharacter('<bg=green> </>');
        $progress->setProgressCharacter('<bg=green> </>');
        $progress->setEmptyBarCharacter('<bg=red> </>');
        $progress->start();
        
        $processor = new Processor($client, $progress, $output);
        $data = $processor->process($config);

        $progress->finish();
        
        $rows = [];
        foreach($data['byProject'] as $name => $prj) {
            $row = [$name,$prj['first'],$prj['last'], $prj['workBlocks']['number'], $prj['workBlocks']['largest']] + array_fill(5, count($data['allUsers']), "0");
            foreach($prj['counts'] as $user => $days) {
                $pos = $data['allUsers'][$user] + 5;
                $row[$pos] = count($days);
            }
            $rows[] = $row;
        }
        $table = new Table($output);
        $table->setHeaders(array_merge(["project","first commit", "last commit", "number of blocks", "largest block"], array_keys($data['allUsers'])));
        $table->setRows($rows);

        $output->writeln("<info>Success</info>");
        $table->render();
    }
}

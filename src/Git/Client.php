<?php

namespace Git;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\ExecutableFinder;

class Client
{
    protected $cachePath;
    protected $gitPath;
    
    public function __construct($cachePath, $gitPath = null)
    {
        $this->cachePath = $cachePath;

        if (!$gitPath) {
            $finder = new ExecutableFinder();
            $gitPath = $finder->find('git', '/usr/bin/git');
        }
        
        $this->gitPath = $gitPath;
    }


    /**
     * Get the current Git binary path
     *
     * @return string Path where the Git binary is located
     */
    protected function getGitPath()
    {
        return escapeshellarg($this->gitPath);
    }
    
    public function grab($remote)
    {
        $id = uniqid();
        $path = $this->cachePath . '/' . $id;
        $command = "clone $remote $path";
        $process = new Process($this->getGitPath() . ' ' . $command);
        $process->setTimeout(180);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput());
        }
        return $id;
    }

    public function log($id)
    {
        $path = $this->cachePath . '/' . $id;
        $command = 'log --pretty=format:"%ad|%ae|%s" --decorate --date=short';
        $process = new Process($this->getGitPath() . ' ' . $command, $path);
        $process->setTimeout(180);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput());
        }
        return $process->getOutput();
    }

}